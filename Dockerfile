# STEP 1 build static website
FROM node:12 as builder

# Create app directory
WORKDIR /survey-contacts

COPY package.json  /survey-contacts/

# Copy project files into the docker image
COPY . /survey-contacts/

RUN npm install && npm run build

# STEP 2 build a small nginx image with static website
FROM nginx:alpine

RUN apk add --no-cache curl

RUN rm -rf /usr/share/nginx/html/*

RUN rm /etc/nginx/conf.d/default.conf

COPY nginx/default.conf  /etc/nginx/nginx.conf

## From 'builder' copy website to default nginx public folder
COPY --from=builder /survey-contacts/dist/survey-contacts /usr/share/nginx/html

RUN nginx -v

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
